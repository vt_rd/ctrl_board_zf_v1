################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../sourceFiles/framework/fw_adc.c \
../sourceFiles/framework/fw_homeSensor.c \
../sourceFiles/framework/fw_ledIndicator.c \
../sourceFiles/framework/fw_motor.c \
../sourceFiles/framework/fw_pwm.c \
../sourceFiles/framework/fw_qep.c \
../sourceFiles/framework/fw_sci.c \
../sourceFiles/framework/fw_timer.c 

C_DEPS += \
./sourceFiles/framework/fw_adc.d \
./sourceFiles/framework/fw_homeSensor.d \
./sourceFiles/framework/fw_ledIndicator.d \
./sourceFiles/framework/fw_motor.d \
./sourceFiles/framework/fw_pwm.d \
./sourceFiles/framework/fw_qep.d \
./sourceFiles/framework/fw_sci.d \
./sourceFiles/framework/fw_timer.d 

OBJS += \
./sourceFiles/framework/fw_adc.obj \
./sourceFiles/framework/fw_homeSensor.obj \
./sourceFiles/framework/fw_ledIndicator.obj \
./sourceFiles/framework/fw_motor.obj \
./sourceFiles/framework/fw_pwm.obj \
./sourceFiles/framework/fw_qep.obj \
./sourceFiles/framework/fw_sci.obj \
./sourceFiles/framework/fw_timer.obj 

OBJS__QUOTED += \
"sourceFiles\framework\fw_adc.obj" \
"sourceFiles\framework\fw_homeSensor.obj" \
"sourceFiles\framework\fw_ledIndicator.obj" \
"sourceFiles\framework\fw_motor.obj" \
"sourceFiles\framework\fw_pwm.obj" \
"sourceFiles\framework\fw_qep.obj" \
"sourceFiles\framework\fw_sci.obj" \
"sourceFiles\framework\fw_timer.obj" 

C_DEPS__QUOTED += \
"sourceFiles\framework\fw_adc.d" \
"sourceFiles\framework\fw_homeSensor.d" \
"sourceFiles\framework\fw_ledIndicator.d" \
"sourceFiles\framework\fw_motor.d" \
"sourceFiles\framework\fw_pwm.d" \
"sourceFiles\framework\fw_qep.d" \
"sourceFiles\framework\fw_sci.d" \
"sourceFiles\framework\fw_timer.d" 

C_SRCS__QUOTED += \
"../sourceFiles/framework/fw_adc.c" \
"../sourceFiles/framework/fw_homeSensor.c" \
"../sourceFiles/framework/fw_ledIndicator.c" \
"../sourceFiles/framework/fw_motor.c" \
"../sourceFiles/framework/fw_pwm.c" \
"../sourceFiles/framework/fw_qep.c" \
"../sourceFiles/framework/fw_sci.c" \
"../sourceFiles/framework/fw_timer.c" 


