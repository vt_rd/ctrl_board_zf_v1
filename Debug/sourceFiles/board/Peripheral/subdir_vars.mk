################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../sourceFiles/board/Peripheral/homeSensor.c \
../sourceFiles/board/Peripheral/ledIndicator.c \
../sourceFiles/board/Peripheral/lm35.c \
../sourceFiles/board/Peripheral/motor.c 

C_DEPS += \
./sourceFiles/board/Peripheral/homeSensor.d \
./sourceFiles/board/Peripheral/ledIndicator.d \
./sourceFiles/board/Peripheral/lm35.d \
./sourceFiles/board/Peripheral/motor.d 

OBJS += \
./sourceFiles/board/Peripheral/homeSensor.obj \
./sourceFiles/board/Peripheral/ledIndicator.obj \
./sourceFiles/board/Peripheral/lm35.obj \
./sourceFiles/board/Peripheral/motor.obj 

OBJS__QUOTED += \
"sourceFiles\board\Peripheral\homeSensor.obj" \
"sourceFiles\board\Peripheral\ledIndicator.obj" \
"sourceFiles\board\Peripheral\lm35.obj" \
"sourceFiles\board\Peripheral\motor.obj" 

C_DEPS__QUOTED += \
"sourceFiles\board\Peripheral\homeSensor.d" \
"sourceFiles\board\Peripheral\ledIndicator.d" \
"sourceFiles\board\Peripheral\lm35.d" \
"sourceFiles\board\Peripheral\motor.d" 

C_SRCS__QUOTED += \
"../sourceFiles/board/Peripheral/homeSensor.c" \
"../sourceFiles/board/Peripheral/ledIndicator.c" \
"../sourceFiles/board/Peripheral/lm35.c" \
"../sourceFiles/board/Peripheral/motor.c" 


