################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../sourceFiles/system/F2806x_CodeStartBranch.asm \
../sourceFiles/system/F2806x_usDelay.asm 

C_SRCS += \
../sourceFiles/system/F2806x_Adc.c \
../sourceFiles/system/F2806x_CpuTimers.c \
../sourceFiles/system/F2806x_DefaultIsr.c \
../sourceFiles/system/F2806x_EPwm.c \
../sourceFiles/system/F2806x_EQep.c \
../sourceFiles/system/F2806x_GlobalVariableDefs.c \
../sourceFiles/system/F2806x_Gpio.c \
../sourceFiles/system/F2806x_PieCtrl.c \
../sourceFiles/system/F2806x_PieVect.c \
../sourceFiles/system/F2806x_Sci.c \
../sourceFiles/system/F2806x_SysCtrl.c 

C_DEPS += \
./sourceFiles/system/F2806x_Adc.d \
./sourceFiles/system/F2806x_CpuTimers.d \
./sourceFiles/system/F2806x_DefaultIsr.d \
./sourceFiles/system/F2806x_EPwm.d \
./sourceFiles/system/F2806x_EQep.d \
./sourceFiles/system/F2806x_GlobalVariableDefs.d \
./sourceFiles/system/F2806x_Gpio.d \
./sourceFiles/system/F2806x_PieCtrl.d \
./sourceFiles/system/F2806x_PieVect.d \
./sourceFiles/system/F2806x_Sci.d \
./sourceFiles/system/F2806x_SysCtrl.d 

OBJS += \
./sourceFiles/system/F2806x_Adc.obj \
./sourceFiles/system/F2806x_CodeStartBranch.obj \
./sourceFiles/system/F2806x_CpuTimers.obj \
./sourceFiles/system/F2806x_DefaultIsr.obj \
./sourceFiles/system/F2806x_EPwm.obj \
./sourceFiles/system/F2806x_EQep.obj \
./sourceFiles/system/F2806x_GlobalVariableDefs.obj \
./sourceFiles/system/F2806x_Gpio.obj \
./sourceFiles/system/F2806x_PieCtrl.obj \
./sourceFiles/system/F2806x_PieVect.obj \
./sourceFiles/system/F2806x_Sci.obj \
./sourceFiles/system/F2806x_SysCtrl.obj \
./sourceFiles/system/F2806x_usDelay.obj 

ASM_DEPS += \
./sourceFiles/system/F2806x_CodeStartBranch.d \
./sourceFiles/system/F2806x_usDelay.d 

OBJS__QUOTED += \
"sourceFiles\system\F2806x_Adc.obj" \
"sourceFiles\system\F2806x_CodeStartBranch.obj" \
"sourceFiles\system\F2806x_CpuTimers.obj" \
"sourceFiles\system\F2806x_DefaultIsr.obj" \
"sourceFiles\system\F2806x_EPwm.obj" \
"sourceFiles\system\F2806x_EQep.obj" \
"sourceFiles\system\F2806x_GlobalVariableDefs.obj" \
"sourceFiles\system\F2806x_Gpio.obj" \
"sourceFiles\system\F2806x_PieCtrl.obj" \
"sourceFiles\system\F2806x_PieVect.obj" \
"sourceFiles\system\F2806x_Sci.obj" \
"sourceFiles\system\F2806x_SysCtrl.obj" \
"sourceFiles\system\F2806x_usDelay.obj" 

C_DEPS__QUOTED += \
"sourceFiles\system\F2806x_Adc.d" \
"sourceFiles\system\F2806x_CpuTimers.d" \
"sourceFiles\system\F2806x_DefaultIsr.d" \
"sourceFiles\system\F2806x_EPwm.d" \
"sourceFiles\system\F2806x_EQep.d" \
"sourceFiles\system\F2806x_GlobalVariableDefs.d" \
"sourceFiles\system\F2806x_Gpio.d" \
"sourceFiles\system\F2806x_PieCtrl.d" \
"sourceFiles\system\F2806x_PieVect.d" \
"sourceFiles\system\F2806x_Sci.d" \
"sourceFiles\system\F2806x_SysCtrl.d" 

ASM_DEPS__QUOTED += \
"sourceFiles\system\F2806x_CodeStartBranch.d" \
"sourceFiles\system\F2806x_usDelay.d" 

C_SRCS__QUOTED += \
"../sourceFiles/system/F2806x_Adc.c" \
"../sourceFiles/system/F2806x_CpuTimers.c" \
"../sourceFiles/system/F2806x_DefaultIsr.c" \
"../sourceFiles/system/F2806x_EPwm.c" \
"../sourceFiles/system/F2806x_EQep.c" \
"../sourceFiles/system/F2806x_GlobalVariableDefs.c" \
"../sourceFiles/system/F2806x_Gpio.c" \
"../sourceFiles/system/F2806x_PieCtrl.c" \
"../sourceFiles/system/F2806x_PieVect.c" \
"../sourceFiles/system/F2806x_Sci.c" \
"../sourceFiles/system/F2806x_SysCtrl.c" 

ASM_SRCS__QUOTED += \
"../sourceFiles/system/F2806x_CodeStartBranch.asm" \
"../sourceFiles/system/F2806x_usDelay.asm" 


