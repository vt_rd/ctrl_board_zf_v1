/*
 * board.h
 *
 *  Created on: May 24, 2018
 *      Author: huynq70
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "DSP28x_Project.h"
#include "typedefs.h"
#include "timer.h"
#include "fw_pwm.h"
#include "fw_qep.h"
#include "fw_sci.h"
#include "fw_adc.h"
#include "fw_timer.h"
#include "lm35.h"
#include "ledIndicator.h"
#include "homeSensor.h"
#include "motor.h"

/*Define PWM*/
#define EPWM_PERIOUS_DEFAULT    50000
#define EPWM_DUTY_DEFAULT       0
#define EPWM_DUTY_MAX           50000
#define EPWM_DUTY_MIN           0
#define EPWM_DUTY_ADJUST        10      /* % */
/*End define PWM*/

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
interrupt void SysTick_Handler(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
interrupt void Speed_Handler(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void BOARD_Init(void);

#endif /* BOARD_H_ */
