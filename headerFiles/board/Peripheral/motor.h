/*
 * motor.h
 *
 *  Created on: Jun 18, 2018
 *      Author: huynq70
 */

#ifndef MOTOR_H_
#define MOTOR_H_

typedef enum motorName
{
    MOTOR_ZOOM              = 0x00,
    MOTOR_FOCUS             = 0x01
}motorName_t;

typedef enum motorCmd
{
    MOTOR_ENABLE            = 0x00,
    MOTOR_DISABLE           = 0x01,
    MOTOR_CW                = 0x02,
    MOTOR_CCW               = 0x03,
    MOTOR_MAX_SPEED         = 0x04,
    MOTOR_MIN_SPEED         = 0x05,
    MOTOR_INCREASE_SPEED    = 0x06,
    MOTOR_DECREASE_SPEED    = 0x07,
    MOTOR_SPEED_10          = 0x08,
    MOTOR_SPEED_20          = 0x09,
    MOTOR_SPEED_30          = 0x10,
    MOTOR_SPEED_40          = 0x11,
    MOTOR_SPEED_50          = 0x12,
    MOTOR_SPEED_60          = 0x13,
    MOTOR_SPEED_70          = 0x14,
    MOTOR_SPEED_80          = 0x15,
    MOTOR_SPEED_90          = 0x16
}motorCmd_t;

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_InitGPIO(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_SetPeriod(void_p motor, void_p period);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_SetDuty(void_p motor, void_p duty);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_SetSpeedMax(void_p motor, void_p speedMax);
/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
BOOL MOTOR_StabilizePosition(void_p motor, void_p des_position);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_StabilizeSpeed(void_p motor, void_p des_speed);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_Speed(void_p motor, void_p speed);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_IncreaseSpeed(void_p motor, void_p px);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_DecreaseSpeed(void_p motor, void_p px);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_MaxSpeed(void_p motor, void_p px);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_MinSpeed(void_p motor, void_p px);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_Direction(void_p motor, void_p dir);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void MOTOR_Enable(void_p motor, void_p state);

#endif /* MOTOR_H_ */
