/*
 * temperature.h
 *
 *  Created on: Jul 13, 2018
 *      Author: Quang Huy
 */

#ifndef _LM35_H_
#define _LM35_H_

#include "typedefs.h"

u16_t LM35_GetTemperature0(void);

u16_t LM35_GetTemperature1(void);

#endif /* _LM35_H_ */
