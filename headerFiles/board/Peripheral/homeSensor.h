/*
 * homeSensor.h
 *
 *  Created on: Jul 13, 2018
 *      Author: Quang Huy
 */

#ifndef _HOMESENSOR_H_
#define _HOMESENSOR_H_

void HOME_FOCUS_InitGPIO(void);

void HOME_FOCUS_IsHome(void_p px1, void_p px2);

void HOME_ZOOM_InitGPIO(void);

void HOME_ZOOM_IsHome(void_p px1, void_p px2);

#endif /* _HOMESENSOR_H_ */
