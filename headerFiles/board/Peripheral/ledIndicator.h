/*
 * ledIndicator.h
 *
 *  Created on: Jul 13, 2018
 *      Author: Quang Huy
 */

#ifndef _LEDINDICATOR_H_
#define _LEDINDICATOR_H_


void LED_InitGPIO(void);

void LED_On(void);

void LED_Off(void);

void LED_Toggle(void);

void LED1_On(void);

void LED1_Off(void);

void LED1_Toggle(void);

void LED2_On(void);

void LED2_Off(void);

void LED2_Toggle(void);

void LED3_On(void);

void LED3_Off(void);

void LED3_Toggle(void);

void LED4_On(void);

void LED4_Off(void);

void LED4_Toggle(void);


#endif /* _LEDINDICATOR_H_ */
