/*
 * fw_timer.h
 *
 *  Created on: Jul 6, 2018
 *      Author: Quang Huy
 */

#ifndef FW_TIMER_H_
#define FW_TIMER_H_

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void CPU_TIMER1_Configure(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void CPU_TIMER2_Configure(void);

#endif /* FW_TIMER_H_ */
