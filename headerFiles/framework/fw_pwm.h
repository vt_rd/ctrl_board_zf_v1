/*
 * fw_pwm.h
 *
 *  Created on: Jul 6, 2018
 *      Author: Quang Huy
 */

#ifndef FW_PWM_H_
#define FW_PWM_H_

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void PWM1_InitGPIO(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void PWM1_Configure(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void PWM1_SetPeriod(u16_t period);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void PWM1_SetDuty(u16_t duty);

#endif /* FW_PWM_H_ */
