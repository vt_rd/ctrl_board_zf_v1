/*
 * uart.h
 *
 *  Created on: Jun 18, 2018
 *      Author: huynq70
 */

#ifndef FW_SCI_H_
#define FW_SCI_H_

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void SCIA_InitGPIO(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void SCIA_Configure(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void SCIA_SendChar(Uint16 a);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void SCIA_SendString(char *msg);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void SCIA_SendInt(Uint32 number);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void SCIA_SendNL(void);
/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
interrupt void SCIA_RxFifoIsr(void);

#endif /* FW_SCI_H_ */
