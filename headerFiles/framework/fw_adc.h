/*
 * drv_adc.h
 *
 *  Created on: Jun 28, 2018
 *      Author: huynq70
 */

#ifndef FW_ADC_H_
#define FW_ADC_H_

#include "typedefs.h"

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void ADC_Configure(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
u16_t ADC_GetResult0(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
u16_t ADC_GetResult1(void);

#endif /* DRV_ADC_H_ */
