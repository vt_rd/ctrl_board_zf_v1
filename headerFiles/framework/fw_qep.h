/*
 * PosSpeed.h
 *
 *  Created on: Jun 16, 2018
 *      Author: huynq70
 */

#ifndef FW_QEP_H_
#define FW_QEP_H_

#include "IQmathLib.h"

#define POSSPEED_DEFAULTS {0x0, 0x0,0x0,0x0,0x0,209715,2,0,0x0,\
    1406,0,30000,0,\
    0,0,0,0}

typedef struct PosSpeed
{
    i16 theta_elec;                 /*Output: Motor Electrical angle (Q15)*/
    i16 theta_mech;                 /*Output: Motor Mechanical Angle (Q15)*/
    i16 DirectionQep;               /*Output: Motor rotation direction (Q0)*/
    i16 QEP_cnt_idx;                /*Variable: Encoder counter index (Q0)*/
    i16 theta_raw;                  /*Variable: Raw angle from Timer 2 (Q0)*/

    /*
     * Parameter: 0.9999/total count, total count = 4000 (Q26)
     * */
    i32 mech_scaler;
    i16 pole_pairs;                 /*Parameter: Number of pole pairs (Q0)*/

    /*
     * Parameter: Raw angular offset between encoder and phase a (Q0)
     * */
    i16 cal_angle;
    u32_t index_sync_flag;          /*Output: Index sync status (Q0)*/

    /*
     * Parameter :  Scaler converting 1/N cycles to a GLOBAL_Q speed (Q0)
     * independently with global Q
     * */
    u32_t SpeedScaler;
    _iq Speed_pr;                   /*Output :  speed in per-unit*/

    /*
     * Parameter : Scaler converting GLOBAL_Q speed to rpm (Q0) speed
     * independently with global Q
     * */
    u32_t BaseRpm;

    /*
     * Output : speed in r.p.m. (Q0) - independently with global Q
     * */
    i32 SpeedRpm_pr;

    _iq  oldpos;                    /*Input: Electrical angle (pu)*/
    _iq Speed_fr;                   /*Output :  speed in per-unit*/

    /*
     * Output : Speed in rpm  (Q0) - independently with global Q
     * */
    i32 SpeedRpm_fr;

    u32_t pulseCount;               /*Output:*/
} PosSpeed_t;

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void QEP1_InitGPIO(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void QEP1_Configure(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
Direction_t QEP1_GetDirection(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void QEP1_PosSpeedCalculate(PosSpeed_t *posSpeed);

#endif /* FW_QEP_H_ */
