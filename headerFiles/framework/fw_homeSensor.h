/*
 * fw_homeSensor.h
 *
 *  Created on: Jul 13, 2018
 *      Author: Quang Huy
 */

#ifndef _FW_HOMESENSOR_H_
#define _FW_HOMESENSOR_H_

#include "typedefs.h"

void FW_HOME_FOCUS_InitGPIO(void);

BOOL FW_HOME_FOCUS_IsHome(void);

void FW_HOME_ZOOM_InitGPIO(void);

BOOL FW_HOME_ZOOM_IsHome(void);

#endif /* _FW_HOMESENSOR_H_ */
