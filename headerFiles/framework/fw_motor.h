/*
 * fw_motor.h
 *
 *  Created on: Jul 6, 2018
 *      Author: Quang Huy
 */

#ifndef FW_MOTOR_H_
#define FW_MOTOR_H_

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void FW_MOTOR_FOCUS_InitGPIO(void);

void FW_MOTOR_FOCUS_Enable(void);

void FW_MOTOR_FOCUS_Disable(void);

void FW_MOTOR_FOCUS_CW(void);

void FW_MOTOR_FOCUS_CCW(void);

void FW_MOTOR_ZOOM_InitGPIO(void);

void FW_MOTOR_ZOOM_Enable(void);

void FW_MOTOR_ZOOM_Disable(void);

void FW_MOTOR_ZOOM_CW(void);

void FW_MOTOR_ZOOM_CCW(void);

#endif /* FW_MOTOR_H_ */
