/*
 * fw_ledIndicate.h
 *
 *  Created on: Jul 6, 2018
 *      Author: Quang Huy
 */

#ifndef FW_LEDINDICATOR_H_
#define FW_LEDINDICATOR_H_

void FW_LED_InitGPIO(void);

void FW_LED_On(void);

void FW_LED_Off(void);

void FW_LED_Toggle(void);

void FW_LED1_On(void);

void FW_LED1_Off(void);

void FW_LED1_Toggle(void);

void FW_LED2_On(void);

void FW_LED2_Off(void);

void FW_LED2_Toggle(void);

void FW_LED3_On(void);

void FW_LED3_Off(void);

void FW_LED3_Toggle(void);

void FW_LED4_On(void);

void FW_LED4_Off(void);

void FW_LED4_Toggle(void);

#endif /* FW_LEDINDICATOR_H_ */
