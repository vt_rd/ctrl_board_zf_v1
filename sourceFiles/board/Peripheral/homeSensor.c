/*
 * homeSensor.c
 *
 *  Created on: Jul 13, 2018
 *      Author: Quang Huy
 */
#include "board.h"
#include "fw_homeSensor.h"
#include "homeSensor.h"

/********************************************************************************/
/*                            HOME_FOCUS                                        */
/********************************************************************************/
void HOME_FOCUS_InitGPIO(void)
{
    FW_HOME_FOCUS_InitGPIO();
}

void HOME_FOCUS_IsHome(void_p px1, void_p px2)
{
    if(FW_HOME_FOCUS_IsHome())
    {
        MOTOR_Enable((void_p)MOTOR_ZOOM, (void_p)MOTOR_DISABLE);
    }
}

/********************************************************************************/
/*                            HOME_ZOOM                                         */
/********************************************************************************/
void HOME_ZOOM_InitGPIO(void)
{
    FW_HOME_ZOOM_InitGPIO();
}

void HOME_ZOOM_IsHome(void_p px1, void_p px2)
{

}
