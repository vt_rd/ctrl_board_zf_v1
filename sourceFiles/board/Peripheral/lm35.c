/*
 * temperature.c
 *
 *  Created on: Jul 13, 2018
 *      Author: Quang Huy
 */
#include "fw_adc.h"
#include "lm35.h"

u16_t LM35_GetTemperature0(void)
{
    return (u32_t)ADC_GetResult0() * 330 / 4096 - 7;
}

u16_t LM35_GetTemperature1(void)
{
    return (u32_t)ADC_GetResult1() * 330 / 4096 - 7;
}
