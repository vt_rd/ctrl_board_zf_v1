/******************************************************************************
 *
 * Copyright (c) 2018
 * Viettel R&D.
 * All Rights Reserved
 *
 *
 * API for control motor
 *
 * Author:  QuangHuy
 *
 * Last Changed By:  $Author:     QuangHuy
 * Revision:         $Revision:   1.0 $
 * Last Changed:     By QuangHuy  $Date: 18/6/2018     11:59:00
 *
 ******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/
#include "board.h"
#include "fw_motor.h"
#include "motor.h"
/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/
#ifdef  MOTOR
#define MOTOR_SEND_CHAR(c)        SCIA_SendChar(c)
#define MOTOR_SEND_STR(STR)       SCIA_SendString(STR)
#define MOTOR_SEND_NUM(num)       SCIA_SendInt(num)
#define MOTOR_SEND_NL()           SCIA_SendNL()
#else
#define MOTOR_SEND_CHAR(c)
#define MOTOR_SEND_STR(STR)
#define MOTOR_SEND_NUM(num)
#define MOTOR_SEND_NL()
#endif
/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/
static i32 g_motorZoomSpeed = EPWM_DUTY_DEFAULT;
//static u8_t g_motorFocusSpeed = EPWM_DUTY_DEFAULT;
static motorCmd_t g_motorZoomDirection = MOTOR_CW;
//static motorCmd_t g_motorFocusDirection = MOTOR_CW;
static u16_t g_motorZoomSpeedMax = 10000;
//static u16_t g_motorFocusSpeedMax = 0;
/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/
PosSpeed_t qep_posspeed = POSSPEED_DEFAULTS;



#define Sampling_time       1        /* us */
#define inv_Sampling_time   1000000    /*1/Sampling_time*/
static i32 real_speed = 0;
static i32 Err = 0, pre_Err = 0, Kp = 5, Ki = 2700, Kd = 0;
static i32 pPart = 0, iPart = 0, dPart = 0;
static i32 Output = 0;

#define POSITION_SAMPLING_TIME       1          /* us */
#define POSITION_INV_SAMPLING_TIME   1000000    /*1/Sampling_time*/
static i32 real_position = 0;
static i32 err_position = 0, pre_err_position = 0;
static i32 kp_position = 20, ki_position = 0, kd_position = 0;    //kp = 500, ki = 10000
static i32 pPart_position = 0, iPart_position = 0, dPart_position = 0;
static i32 output_position = 0;
/******************************************************************************/
/*                            PRIVATE FUNCTIONS                               */
/******************************************************************************/
/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
static void MOTOR_SendDutyCycleToPC(u32_t dutyCycle);
/******************************************************************************/
/*                            EXPORTED FUNCTIONS                              */
/******************************************************************************/

/******************************************************************************/
static void MOTOR_SendDutyCycleToPC(u32_t dutyCycle)
{
    MOTOR_SEND_NUM((dutyCycle * 100) / EPWM_PERIOUS_DEFAULT);
    MOTOR_SEND_CHAR('%');
}

void MOTOR_InitGPIO(void)
{
    FW_MOTOR_ZOOM_InitGPIO();
    FW_MOTOR_FOCUS_InitGPIO();
}

void MOTOR_SetPeriod(void_p motor, void_p period)
{
    motorName_t m_motor = (motorName_t)motor;
    u16_t m_period = (u16_t)period;

    switch(m_motor)
    {
        case MOTOR_ZOOM:
            PWM1_SetPeriod(m_period);
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_SetDuty(void_p motor, void_p duty)
{
    motorName_t m_motor = (motorName_t)motor;
    u16_t m_duty = (u16_t)duty;

    switch(m_motor)
    {
        case MOTOR_ZOOM:
            PWM1_SetDuty(m_duty);
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_SetSpeedMax(void_p motor, void_p speedMax)
{
    motorName_t m_motor = (motorName_t)motor;
    u16_t m_speedMax = (u16_t)speedMax;

    switch(m_motor)
    {
        case MOTOR_ZOOM:
            g_motorZoomSpeedMax = m_speedMax;
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

BOOL MOTOR_StabilizePosition(void_p motor, void_p des_position)
{
    motorName_t m_motor = (motorName_t)motor;
    i32 m_des_position = (i32)des_position;

    QEP1_PosSpeedCalculate(&qep_posspeed);

    real_position = qep_posspeed.pulseCount;

    switch (m_motor)
    {
        case MOTOR_ZOOM:
            {
                err_position = m_des_position - real_position;

                /*PID Posiion*/
                pPart_position = kp_position * err_position;
                iPart_position += ki_position * POSITION_SAMPLING_TIME * pre_err_position / 1000000;
                dPart_position = kd_position * (err_position - pre_err_position) * POSITION_INV_SAMPLING_TIME;

                output_position = pPart_position + dPart_position + iPart_position;

                if(output_position >= 0)
                {
                    /*Quay chieu thuan ClockWise*/
                    MOTOR_Direction((void_p)MOTOR_ZOOM, (void_p)MOTOR_CW);
                }
                else
                {
                    /*Quay chieu nghich CounterClockWise*/
                    MOTOR_Direction((void_p)MOTOR_ZOOM, (void_p)MOTOR_CCW);
                    output_position = -output_position;
                }

                if(output_position >= g_motorZoomSpeedMax) output_position = g_motorZoomSpeedMax;
                if(output_position <= 0) output_position = 0;

                pre_err_position = err_position;

                MOTOR_StabilizeSpeed((void_p)MOTOR_ZOOM, (void_p)output_position);
            }
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
    return TRUE;
}

/*Functions for control motor*/
void MOTOR_StabilizeSpeed(void_p motor, void_p des_speed)
{
    motorName_t m_motor = (motorName_t)motor;
    i32 m_des_speed = (i32)des_speed;

    QEP1_PosSpeedCalculate(&qep_posspeed);

    real_speed = qep_posspeed.SpeedRpm_fr;

    if(real_speed >= 0)
    {
        /*Quay chieu thuan ClockWise*/
    }
    else
    {
        /*Quay chieu nghich CounterClockWise*/
        real_speed = -real_speed;
    }

    switch (m_motor)
    {
        case MOTOR_ZOOM:
            {
                Err = m_des_speed - real_speed;

                /*PID*/
                pPart = Kp * Err;
                iPart += Ki * Sampling_time * pre_Err / 1000000;
                dPart = Kd * (Err - pre_Err) * inv_Sampling_time;

                Output = pPart + dPart + iPart;

                if(Output >= EPWM_DUTY_MAX) Output = EPWM_DUTY_MAX - 1;
                if(Output <= EPWM_DUTY_MIN) Output = EPWM_DUTY_MIN + 1;

                pre_Err = Err;

                if(g_motorZoomDirection == MOTOR_CW)
                {
                    /*Do nothing*/
                }
                else    /*MOTOR_CCW*/
                {
                    Output = EPWM_DUTY_MAX - Output;
                }
                MOTOR_SetDuty((void_p)MOTOR_ZOOM, (void_p)Output);
            }
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_Speed(void_p motor, void_p speed)
{
    motorName_t m_motor = (motorName_t)motor;
    u8_t m_speed = (u8_t)speed;

    switch (m_motor)
    {
        case MOTOR_ZOOM:
            if(g_motorZoomDirection == MOTOR_CW)
            {
                g_motorZoomSpeed = m_speed * (EPWM_DUTY_MAX / 100);
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }
            else
            {
                g_motorZoomSpeed = EPWM_DUTY_MAX - (m_speed * (EPWM_DUTY_MAX / 100));
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(EPWM_DUTY_MAX - g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }

            MOTOR_SetDuty((void_p)MOTOR_ZOOM, (void_p)g_motorZoomSpeed);
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_IncreaseSpeed(void_p motor, void_p px)
{
    motorName_t m_motor = (motorName_t)motor;

    switch (m_motor)
    {
        case MOTOR_ZOOM:
            if(g_motorZoomDirection == MOTOR_CW)
            {
                g_motorZoomSpeed += EPWM_DUTY_ADJUST * (EPWM_DUTY_MAX / 100);
                if(g_motorZoomSpeed >= EPWM_DUTY_MAX) g_motorZoomSpeed = EPWM_DUTY_MAX;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }
            else
            {
                g_motorZoomSpeed -= EPWM_DUTY_ADJUST * (EPWM_DUTY_MAX / 100);
                if(g_motorZoomSpeed <= EPWM_DUTY_MIN) g_motorZoomSpeed = EPWM_DUTY_MIN;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(EPWM_DUTY_MAX - g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }

            MOTOR_SetDuty((void_p)MOTOR_ZOOM, (void_p)g_motorZoomSpeed);
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_DecreaseSpeed(void_p motor, void_p px)
{
    motorName_t m_motor = (motorName_t)motor;

    switch (m_motor)
    {
        case MOTOR_ZOOM:
            if(g_motorZoomDirection == MOTOR_CW)
            {
                g_motorZoomSpeed -= EPWM_DUTY_ADJUST * (EPWM_DUTY_MAX / 100);
                if(g_motorZoomSpeed <= EPWM_DUTY_MIN) g_motorZoomSpeed = EPWM_DUTY_MIN;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }
            else
            {
                g_motorZoomSpeed += EPWM_DUTY_ADJUST * (EPWM_DUTY_MAX / 100);
                if(g_motorZoomSpeed >= EPWM_DUTY_MAX) g_motorZoomSpeed = EPWM_DUTY_MAX;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(EPWM_DUTY_MAX - g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }

            MOTOR_SetDuty((void_p)MOTOR_ZOOM, (void_p)g_motorZoomSpeed);
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_MaxSpeed(void_p motor, void_p px)
{
    motorName_t m_motor = (motorName_t)motor;

    switch (m_motor)
    {
        case MOTOR_ZOOM:
            if(g_motorZoomDirection == MOTOR_CW)
            {
                g_motorZoomSpeed = EPWM_DUTY_MAX;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }
            else
            {
                g_motorZoomSpeed = EPWM_DUTY_MIN;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(EPWM_DUTY_MAX - g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }

            MOTOR_SetDuty((void_p)MOTOR_ZOOM, (void_p)g_motorZoomSpeed);
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_MinSpeed(void_p motor, void_p px)
{
    motorName_t m_motor = (motorName_t)motor;

    switch (m_motor)
    {
        case MOTOR_ZOOM:
            if(g_motorZoomDirection == MOTOR_CW)
            {
                g_motorZoomSpeed = EPWM_DUTY_MIN;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }
            else
            {
                g_motorZoomSpeed = EPWM_DUTY_MAX;
                MOTOR_SEND_STR("ZOOM Motor Speed: ");
                MOTOR_SendDutyCycleToPC(EPWM_DUTY_MAX - g_motorZoomSpeed);
                MOTOR_SEND_NL();
            }

            MOTOR_SetDuty((void_p)MOTOR_ZOOM, (void_p)g_motorZoomSpeed);
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_Direction(void_p motor, void_p dir)
{
    motorCmd_t m_dir = (motorCmd_t)dir;
    motorName_t m_motor = (motorName_t)motor;

    switch (m_motor) {
        case MOTOR_ZOOM:
            if(m_dir == MOTOR_CW)
            {
                g_motorZoomDirection = MOTOR_CW;
                FW_MOTOR_ZOOM_CW();
                //MOTOR_Enable((void_p)MOTOR_ZOOM, (void_p)MOTOR_DISABLE);
                //MOTOR_MinSpeed((void_p)MOTOR_ZOOM, NULL);
                //MOTOR_SEND_STR("Motor ZOOM Direction: Clockwise\n");
            }
            else
            {
                g_motorZoomDirection = MOTOR_CCW;
                FW_MOTOR_ZOOM_CCW();
                //MOTOR_Enable((void_p)MOTOR_ZOOM, (void_p)MOTOR_DISABLE);
                //MOTOR_MinSpeed((void_p)MOTOR_ZOOM, NULL);
                //MOTOR_SEND_STR("Motor ZOOM Direction: Counterclockwise\n");
            }
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}

void MOTOR_Enable(void_p motor, void_p state)
{
    motorCmd_t m_state = (motorCmd_t)state;
    motorName_t m_motor = (motorName_t)motor;

    switch (m_motor) {
        case MOTOR_ZOOM:
            if(m_state == MOTOR_ENABLE)
            {
                FW_MOTOR_ZOOM_Enable();
                MOTOR_SEND_STR("Motor ZOOM Enable: Enable\n");
            }
            else
            {
                FW_MOTOR_ZOOM_Disable();
                MOTOR_SEND_STR("Motor ZOOM Enable: Disable\n");
            }
            break;
        case MOTOR_FOCUS:
            break;
        default:
            break;
    }
}
