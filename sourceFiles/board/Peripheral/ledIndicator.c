/*
 * ledIndicator.c
 *
 *  Created on: Jul 13, 2018
 *      Author: Quang Huy
 */
#include "board.h"
#include "fw_ledIndicator.h"
#include "ledIndicator.h"

void LED_InitGPIO(void)
{
    FW_LED_InitGPIO();
}

void LED_On(void)
{
    FW_LED_On();
}

void LED_Off(void)
{
    FW_LED_Off();
}

void LED_Toggle(void)
{
    FW_LED_Toggle();
}

void LED1_On(void)
{
    FW_LED1_On();
}

void LED1_Off(void)
{
    FW_LED1_Off();
}

void LED1_Toggle(void)
{
    FW_LED1_Toggle();
}

void LED2_On(void)
{
    FW_LED2_On();
}

void LED2_Off(void)
{
    FW_LED2_Off();
}

void LED2_Toggle(void)
{
    FW_LED2_Toggle();
}

void LED3_On(void)
{
    FW_LED3_On();
}

void LED3_Off(void)
{
    FW_LED3_Off();
}

void LED3_Toggle(void)
{
    FW_LED3_Toggle();
}

void LED4_On(void)
{
    FW_LED4_On();
}

void LED4_Off(void)
{
    FW_LED4_Off();
}

void LED4_Toggle(void)
{
    FW_LED4_Toggle();
}
