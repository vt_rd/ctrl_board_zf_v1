/******************************************************************************
 *
 * Copyright (c) 2018
 * Viettel R&D.
 * All Rights Reserved
 *
 *
 * Description: Control position, speed of one DC Motor Maxon with response of
 * position and speed from Encoder on the Motor.
 *
 * Author:  QuangHuy
 *
 * Last Changed By:  $Author:     QuangHuy
 * Revision:         $Revision:   1.0 $
 * Last Changed:     By QuangHuy  $Date: 24/5/2018     11:59:00
 *
 ******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/
#include "board.h"
/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/

/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/

/******************************************************************************/
/*                            PRIVATE FUNCTIONS                               */
/******************************************************************************/
/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
static void BOARD_InitGPIO(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
static void BOARD_Configure(void);

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
static void BOARD_InitSUCCESS(void_p px1, void_p px2);
/******************************************************************************/
/*                            EXPORTED FUNCTIONS                              */
/******************************************************************************/

/******************************************************************************/
void BOARD_Init(void)
{
    InitSysCtrl();
    DINT;
    InitPieCtrl();
    IER = 0x0000;
    IFR = 0x0000;
    InitPieVectTable();
    InitGpio();

    BOARD_InitGPIO();
    BOARD_Configure();
    TimerInit();

    /*Indicate that Systems are all ready*/
    SCIA_SendString("Systems are all ready!\n");
    TimerStart(100, 20, BOARD_InitSUCCESS, NULL, NULL);
}

static void BOARD_InitGPIO(void)
{
    SCIA_InitGPIO();
    PWM1_InitGPIO();
    QEP1_InitGPIO();
    MOTOR_InitGPIO();
    HOME_FOCUS_InitGPIO();
    LED_InitGPIO();
}

static void BOARD_Configure(void)
{
    SCIA_Configure();
    PWM1_SetPeriod(EPWM_PERIOUS_DEFAULT);
    PWM1_SetDuty(EPWM_DUTY_DEFAULT);
    PWM1_Configure();
    QEP1_Configure();
    ADC_Configure();
    CPU_TIMER1_Configure();
    CPU_TIMER2_Configure();
}

static void BOARD_InitSUCCESS(void_p px1, void_p px2)
{
    LED_Toggle();
}
