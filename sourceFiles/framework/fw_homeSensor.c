/******************************************************************************
 *
 * Copyright (c) 2018
 * Viettel R&D.
 * All Rights Reserved
 *
 *
 * Description:
 *
 * Author:  QuangHuy
 *
 * Last Changed By:  $Author:     QuangHuy
 * Revision:         $Revision:   1.0 $
 * Last Changed:     By QuangHuy  $Date: 18/6/2018     11:59:00
 *
 ******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/
#include "DSP28x_Project.h"
#include "fw_homeSensor.h"
/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/
Uint8 __attribute__((weak)) TimerStart(
    u32_t wMilSecTick,              /* IN: timer tick */
    u8_t byRepeats,                 /* IN: number of repeater */
    timer_callback callback,        /* IN: callback function*/
    void_p pcallbackData,           /* IN: parameters */
    void_p pcallbackData2           /* IN: parameters */
);

void __attribute__((weak)) HOME_FOCUS_IsHome(void_p px1, void_p px2);
/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/

/******************************************************************************/
/*                            PRIVATE FUNCTIONS                               */
/******************************************************************************/
__interrupt void FW_HOME_FOCUS_Isr(void);

__interrupt void FW_HOME_ZOOM_Isr(void);
/******************************************************************************/
/*                            EXPORTED FUNCTIONS                              */
/******************************************************************************/

/******************************************************************************/

void FW_HOME_FOCUS_InitGPIO(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;

    PieVectTable.XINT1 = &FW_HOME_FOCUS_Isr;

    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;          // Enable the PIE block
    PieCtrlRegs.PIEIER1.bit.INTx4 = 1;          // Enable PIE Group 1 INT4
    IER |= M_INT1;                              // Enable CPU INT1
    EINT;

    GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0;        // GPIO
    GpioCtrlRegs.GPADIR.bit.GPIO6 = 0;         // input
    GpioCtrlRegs.GPAQSEL1.bit.GPIO6 = 0;       // XINT1 Synch to SYSCLKOUT only

    GpioIntRegs.GPIOXINT1SEL.bit.GPIOSEL = 6;

    XIntruptRegs.XINT1CR.bit.POLARITY = 0;      // Falling edge interrupt
    XIntruptRegs.XINT1CR.bit.ENABLE = 1;        // Enable XINT1

    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

BOOL FW_HOME_FOCUS_IsHome(void)
{
    if(GpioDataRegs.GPADAT.bit.GPIO6 == 0)
    {
        return TRUE;
    }
    return FALSE;
}

__interrupt void FW_HOME_FOCUS_Isr(void)
{
    /*Process after 20ms to eliminate noise.*/
    TimerStart(NOISE_TIME, 1, HOME_FOCUS_IsHome, NULL, NULL);

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

void FW_HOME_ZOOM_InitGPIO(void)
{

}

BOOL FW_HOME_ZOOM_IsHome(void)
{
    return FALSE;
}

__interrupt void FW_HOME_ZOOM_Isr(void)
{

}
