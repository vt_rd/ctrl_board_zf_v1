/*
 * drv_adc.c
 *
 *  Created on: Jun 28, 2018
 *      Author: huynq70
 */
#include "DSP28x_Project.h"
#include "fw_adc.h"

void ADC_Configure(void)
{
    InitAdc();

    EALLOW;
    AdcRegs.ADCCTL2.bit.ADCNONOVERLAP = 1;  //Enable non-overlap mode
    AdcRegs.ADCCTL1.bit.TEMPCONV  = 0;      //1: Connect channel A5 internally to the temperature sensor
    AdcRegs.ADCSOC0CTL.bit.CHSEL  = 0;      //0: Set SOC0 channel select to ADCINA0
    AdcRegs.ADCSOC0CTL.bit.ACQPS  = 6;      //Set SOC0 acquisition period to 7 ADCCLK
    AdcRegs.INTSEL1N2.bit.INT1SEL = 0;      //Connect ADCINT0 to EOC1
    AdcRegs.INTSEL1N2.bit.INT1E  =  1;      //Enable ADCINT1

    AdcRegs.ADCSOC1CTL.bit.CHSEL  = 1;      //5: Set SOC0 channel select to ADCINA5
    AdcRegs.ADCSOC1CTL.bit.ACQPS  = 6;      //Set SOC0 acquisition period to 7 ADCCLK
    AdcRegs.INTSEL1N2.bit.INT2SEL = 1;      //Connect ADCINT1 to EOC1
    AdcRegs.INTSEL1N2.bit.INT2E  =  1;      //Enable ADCINT1
    EDIS;
}

u16_t ADC_GetResult0(void)
{
    Uint16 AdcResult0 = 0;
    //Force start of conversion on SOC0
    AdcRegs.ADCSOCFRC1.bit.SOC0 = 1;
    //Wait for end of conversion.
    while(AdcRegs.ADCINTFLG.bit.ADCINT1 == 0){}  //Wait for ADCINT1
    AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;        //Clear ADCINT1
    //Get temp sensor sample result from SOC0
    AdcResult0 = AdcResult.ADCRESULT0;
    return AdcResult0;
}

u16_t ADC_GetResult1(void)
{
    Uint16 AdcResult1 = 0;
    //Force start of conversion on SOC0
    AdcRegs.ADCSOCFRC1.bit.SOC1 = 1;
    //Wait for end of conversion.
    while(AdcRegs.ADCINTFLG.bit.ADCINT2 == 0){}  //Wait for ADCINT1
    AdcRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;        //Clear ADCINT1
    //Get temp sensor sample result from SOC0
    AdcResult1 = AdcResult.ADCRESULT1;
    return AdcResult1;
}
