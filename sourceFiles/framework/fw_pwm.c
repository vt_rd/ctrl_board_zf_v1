/*
 * fw_pwm.c
 *
 *  Created on: Jul 6, 2018
 *      Author: Quang Huy
 */
#include "DSP28x_Project.h"
#include "typedefs.h"
#include "fw_pwm.h"

void PWM1_InitGPIO(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioCtrlRegs.GPAPUD.bit.GPIO0 = 1;      /*Disable pull-up on GPIO0 (EPWM1A), reduce power consumption*/
    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1;     /*Configure GPIO0 as EPWM1A*/
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void PWM1_Configure(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;

    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;      /*Count up mode             */
    //EPwm1Regs.TBPRD = 50000;                        /*Set ePWM Period           */
    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;         /*Disable phase loading     */
    EPwm1Regs.TBPHS.half.TBPHS = 0x0000;            /*Phase is 0                */
    EPwm1Regs.TBCTR = 0x0000;                       /*Clear counter             */
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;        /*Clock ratio to SYSCLKOUT  */
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;

    //EPwm1Regs.CMPA.half.CMPA = 0;                   /*Set compare A value       */

    EPwm1Regs.AQCTLA.bit.ZRO = AQ_SET;              /*Set output ePWM high when TBCTR == 0*/
    EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;            /*Set output ePWM low when TBCTR == CMPA*/
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;

    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

/*
 * Chen them cong thuc tinh Period theo HSPCLKDIV va CLKDIV
 *
 * */
void PWM1_SetPeriod(u16_t period)
{
    EALLOW;
    EPwm1Regs.TBPRD = period;
    EDIS;
}

/*
 *
 *
 * */
void PWM1_SetDuty(u16_t duty)
{
    EALLOW;
    EPwm1Regs.CMPA.half.CMPA = duty;
    EDIS;
}



