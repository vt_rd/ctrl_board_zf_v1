/******************************************************************************
 *
 * Copyright (c) 2018
 * Viettel R&D.
 * All Rights Reserved
 *
 *
 * Description: SCI
 *
 * Author:  QuangHuy
 *
 * Last Changed By:  $Author:     QuangHuy
 * Revision:         $Revision:   1.0 $
 * Last Changed:     By QuangHuy  $Date: 06/6/2018     11:59:00
 *
 ******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/
#include "DSP28x_Project.h"
#include "typedefs.h"
#include "fw_sci.h"
/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/
Uint8 __attribute__((weak)) TimerStart(
    u32_t wMilSecTick,              /* IN: timer tick */
    u8_t byRepeats,                 /* IN: number of repeater */
    timer_callback callback,        /* IN: callback function*/
    void_p pcallbackData,           /* IN: parameters */
    void_p pcallbackData2           /* IN: parameters */
);
/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/

/******************************************************************************/
/*                            PRIVATE FUNCTIONS                               */
/******************************************************************************/
/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
static void SCIA_InitFIFO(void);
/******************************************************************************/
/*                            EXPORTED FUNCTIONS                              */
/******************************************************************************/
void __attribute__((weak)) CmdHandler(void_p motor, void_p cmd);
/******************************************************************************/
static void SCIA_InitFIFO(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    SciaRegs.SCIFFTX.all = 0xC021;
    SciaRegs.SCIFFRX.all = 0x0021;
    SciaRegs.SCIFFCT.all = 0x0;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void SCIA_InitGPIO(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioCtrlRegs.GPAPUD.bit.GPIO28 = 0;     /*Disable pull-up for GPIO28 (SCIRXDA), reduce power consumption*/
    GpioCtrlRegs.GPAPUD.bit.GPIO29 = 0;     /*Disable pull-up for GPIO29 (SCITXDA), reduce power consumption*/
    GpioCtrlRegs.GPAQSEL2.bit.GPIO28 = 3;   /*Inputs are synchronized to SYSCLKOUT*/
    GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 1;    /*Configure GPIO28 for SCIRXDA operation*/
    GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 1;    /*Configure GPIO29 for SCITXDA operation*/
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void SCIA_Configure(void)
{
    SCIA_InitFIFO();

    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    PieVectTable.SCIRXINTA = &SCIA_RxFifoIsr;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;

    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    SciaRegs.SCICCR.all = 0x0007;
    SciaRegs.SCICTL1.all = 0x0003;
    SciaRegs.SCICTL2.bit.RXBKINTENA = 1;

    SciaRegs.SCIHBAUD = 0x0001;
    SciaRegs.SCILBAUD = 0x0024;

    SciaRegs.SCICTL1.all = 0x0023;
    SciaRegs.SCIFFTX.bit.TXFIFOXRESET = 1;
    SciaRegs.SCIFFRX.bit.RXFIFORESET = 1;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;

    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;   // Enable the PIE block
    PieCtrlRegs.PIEIER9.bit.INTx1=1;     // PIE Group 9, INT1
    IER |= 0x100;                         // Enable CPU INT
    EINT;
}

void SCIA_SendChar(Uint16 a)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    while (SciaRegs.SCIFFTX.bit.TXFFST != 0);
    SciaRegs.SCITXBUF=a;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void SCIA_SendString(char * msg)
{
    int i;
    i = 0;
    while(msg[i] != '\0')
    {
        SCIA_SendChar(msg[i]);
        i++;
    }
}

void SCIA_SendInt(u32_t number)
{
    SCIA_SendChar(((int)number / 100)+48);
    SCIA_SendChar(((int)((int)number / 10) % 10)+48);
    SCIA_SendChar(((int)number % 10)+48);
}

void SCIA_SendNL(void)
{
    SCIA_SendChar('\n');
}

interrupt void SCIA_RxFifoIsr(void)
{
    u8_t rCmd;

    rCmd = SciaRegs.SCIRXBUF.all;  // Read data

    TimerStart(0, 1, CmdHandler, (void_p)0, (void_p)rCmd); /*Motor zoom*/

    SciaRegs.SCIFFRX.bit.RXFFOVRCLR=1;   // Clear Overflow flag
    SciaRegs.SCIFFRX.bit.RXFFINTCLR=1;   // Clear Interrupt flag

    PieCtrlRegs.PIEACK.all|=0x100;       // Issue PIE ack
}
