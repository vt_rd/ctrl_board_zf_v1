/*
 * fw_timer.c
 *
 *  Created on: Jul 6, 2018
 *      Author: Quang Huy
 */

#include "DSP28x_Project.h"
#include "typedefs.h"
#include "fw_timer.h"

void __attribute__((weak)) Speed_Handler(void);
void __attribute__((weak)) SysTick_Handler(void);

void CPU_TIMER1_Configure(void)
{
    EALLOW;
    PieVectTable.TINT1 = &Speed_Handler;
    EDIS;

    InitCpuTimers();

    ConfigCpuTimer(&CpuTimer1, 90, 100);

    CpuTimer1Regs.TCR.all = 0x4000;

    IER |= M_INT13;

    EINT;   /*Enable Global interrupt INTM*/
    ERTM;   /*Enable Global realtime interrupt DBGM*/
}

void CPU_TIMER2_Configure(void)
{
    EALLOW;
    PieVectTable.TINT2 = &SysTick_Handler;
    EDIS;

    //InitCpuTimers();

    ConfigCpuTimer(&CpuTimer2, 90, 1000);

    CpuTimer2Regs.TCR.all = 0x4000;

    IER |= M_INT14;

    EINT;   /*Enable Global interrupt INTM*/
    ERTM;   /*Enable Global realtime interrupt DBGM*/
}

