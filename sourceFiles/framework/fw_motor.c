/*
 * fw_motor.c
 *
 *  Created on: Jul 6, 2018
 *      Author: Quang Huy
 */
#include "DSP28x_Project.h"
#include "typedefs.h"
#include "fw_motor.h"

/******************************************************************/
/*                    FW_MOTOR_FOCUS                              */
/******************************************************************/
void FW_MOTOR_FOCUS_InitGPIO(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;

    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_FOCUS_Enable(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;

    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_FOCUS_Disable(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;

    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_FOCUS_CW(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;

    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_FOCUS_CCW(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;

    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

/******************************************************************/
/*                    FW_MOTOR_ZOOM                               */
/******************************************************************/
void FW_MOTOR_ZOOM_InitGPIO(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioCtrlRegs.GPADIR.bit.GPIO1 = 1;      /*MOTOR_ZOOM_DIRECTION*/
    GpioCtrlRegs.GPADIR.bit.GPIO15 = 1;     /*MOTOR_ZOOM_ENABLE*/
    GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 0;
    GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 0;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_ZOOM_Enable(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioDataRegs.GPASET.bit.GPIO15 = 1;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_ZOOM_Disable(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioDataRegs.GPACLEAR.bit.GPIO15 = 1;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_ZOOM_CW(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioDataRegs.GPACLEAR.bit.GPIO1 = 1;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void FW_MOTOR_ZOOM_CCW(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioDataRegs.GPASET.bit.GPIO1 = 1;
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}
