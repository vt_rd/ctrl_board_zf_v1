/*
 * PosSpeed.c
 *
 *  Created on: Jun 16, 2018
 *      Author: huynq70
 */
#include "DSP28x_Project.h"
#include "typedefs.h"
#include "fw_qep.h"

void QEP1_InitGPIO(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    GpioCtrlRegs.GPAPUD.bit.GPIO20 = 0;     /*Disable pull-up on GPIO20 (EQEP1A), reduce power consumption*/
    GpioCtrlRegs.GPAPUD.bit.GPIO21 = 0;     /*Disable pull-up on GPIO21 (EQEP1B), reduce power consumption*/
    GpioCtrlRegs.GPAPUD.bit.GPIO22 = 0;     /*Disable pull-up on GPIO22 (EQEP1S), reduce power consumption*/
    GpioCtrlRegs.GPAPUD.bit.GPIO23 = 0;     /*Disable pull-up on GPIO23 (EQEP1I), reduce power consumption*/
    GpioCtrlRegs.GPAQSEL2.bit.GPIO20 = 0;   /*Sync to SYSCLKOUT GPIO20 (EQEP1A) */
    GpioCtrlRegs.GPAQSEL2.bit.GPIO21 = 0;   /*Sync to SYSCLKOUT GPIO21 (EQEP1B) */
    GpioCtrlRegs.GPAQSEL2.bit.GPIO22 = 0;   /*Sync to SYSCLKOUT GPIO22 (EQEP1S) */
    GpioCtrlRegs.GPAQSEL2.bit.GPIO23 = 0;   /*Sync to SYSCLKOUT GPIO23 (EQEP1I) */
    GpioCtrlRegs.GPAMUX2.bit.GPIO20 = 1;    /*Configure GPIO20 as EQEP1A        */
    GpioCtrlRegs.GPAMUX2.bit.GPIO21 = 1;    /*Configure GPIO21 as EQEP1B        */
    GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 1;    /*Configure GPIO22 as EQEP1S        */
    GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 1;    /*Configure GPIO23 as EQEP1I        */
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

void QEP1_Configure(void)
{
    ENABLE_PROTECTED_REGISTER_WRITE_MODE;
    EQep1Regs.QUPRD=900000;                 /*Unit Timer for 100Hz at 90 MHz SYSCLKOUT*/

    EQep1Regs.QDECCTL.bit.QSRC=00;          /*QEP quadrature count mode*/

    EQep1Regs.QEPCTL.bit.FREE_SOFT=2;

    /*
     * PCRM=00 mode - QPOSCNT reset on index event
     * PCRM=01 mode - QPOSCNT reset on the maximum position
     * */
    EQep1Regs.QEPCTL.bit.PCRM=01;

    EQep1Regs.QEPCTL.bit.UTE=1;             /*Unit Timeout Enable*/
    EQep1Regs.QEPCTL.bit.QCLM=1;            /*Latch on unit time out*/
    EQep1Regs.QPOSMAX=0xFFFFFFFF;
    EQep1Regs.QEPCTL.bit.QPEN=1;            /*QEP enable*/

    EQep1Regs.QCAPCTL.bit.UPPS=5;           /*1/32 for unit posnmition*/
    EQep1Regs.QCAPCTL.bit.CCPS=6;           /*1/64 for CAP clock*/
    EQep1Regs.QCAPCTL.bit.CEN=1;            /*QEP Capture Enable*/
    DISABLE_PROTECTED_REGISTER_WRITE_MODE;
}

Direction_t QEP1_GetDirection(void)
{
    Direction_t dir = (Direction_t)EQep1Regs.QEPSTS.bit.QDF;

    return dir;
}

void QEP1_PosSpeedCalculate(PosSpeed_t *posSpeed)
{
    u32_t pos32BVal = 0;
    u32_t tmp_lowSpeed = 0;
    i32 tmp = 0;
    _iq temp_pos32bval;
    _iq new_pos32bval;
    _iq old_pos32bval;

    posSpeed->DirectionQep = EQep1Regs.QEPSTS.bit.QDF;

    pos32BVal = (u32_t)EQep1Regs.QPOSCNT;

    posSpeed->pulseCount = pos32BVal;

    /*
     * Calculate postion
     * */
    posSpeed->index_sync_flag = (pos32BVal / 64);

    posSpeed->theta_mech = ((pos32BVal - (posSpeed->index_sync_flag) * 64) * 360) / 64;

    if (EQep1Regs.QFLG.bit.PCO == 1)
    {
        EQep1Regs.QCLR.bit.PCO = 1;                                     /*Clear interrupt flag*/
    }

    if(EQep1Regs.QFLG.bit.UTO == 1)
    {
        pos32BVal=(u32_t)EQep1Regs.QPOSLAT;                     /*Latched POSCNT value*/
        tmp = (i32)((i32)pos32BVal * (i32)posSpeed->mech_scaler);    /*Q0*Q26 = Q26*/
        tmp &= 0x03FFF000;
        tmp = (i16)(tmp >> 11);                                         /*Q26 -> Q15*/
        tmp &= 0x7FFF;                                                  /*Lay 15 bit*/
        new_pos32bval = _IQ15toIQ(tmp);                                 /*Chuyen _IQ15 thanh _IQ*/
        old_pos32bval = posSpeed->oldpos;

        if(posSpeed->DirectionQep == 0)                                 /*POSCNT is counting down*/
        {
            if (new_pos32bval > old_pos32bval)
            {
                temp_pos32bval = - (_IQ(1) - new_pos32bval + old_pos32bval);    /*x2-x1 should be negative*/
            }
            else
            {
                temp_pos32bval = new_pos32bval - old_pos32bval;
            }
        }
        else if (posSpeed->DirectionQep == 1)                                   /*POSCNT is counting up*/
        {
            if(new_pos32bval < old_pos32bval)
            {
                temp_pos32bval = _IQ(1) + new_pos32bval - old_pos32bval;
            }
            else
            {
                temp_pos32bval = new_pos32bval - old_pos32bval;
            }
        }

        if (temp_pos32bval>_IQ(1))
        {
            posSpeed->Speed_fr = _IQ(1);
        }
        else if (temp_pos32bval < _IQ(-1))
        {
            posSpeed->Speed_fr = _IQ(-1);
        }
        else
        {
            posSpeed->Speed_fr = temp_pos32bval;
        }

        posSpeed->oldpos = new_pos32bval;

        posSpeed->SpeedRpm_fr = _IQmpy(posSpeed->BaseRpm, posSpeed->Speed_fr);

        EQep1Regs.QCLR.bit.UTO = 1;
    }

    /*
     * Low-speed computation using QEP capture counter
     * */
    if(EQep1Regs.QEPSTS.bit.UPEVNT==1)                          /*Unit position event*/
    {
        /*
         * No Capture overflow
         * */
        if(EQep1Regs.QEPSTS.bit.COEF == 0)
        {
            tmp_lowSpeed = (u32_t)EQep1Regs.QCPRDLAT;   /*temp1 = t2-t1*/
        }

        /*
         * Capture overflow, saturate the result
         * */
        else
        {
            tmp_lowSpeed = 0xFFFF;
        }

        /*
         * p->Speed_pr = p->SpeedScaler/temp1
         * */
        posSpeed->Speed_pr = _IQdiv(posSpeed->SpeedScaler, tmp_lowSpeed);
        temp_pos32bval = posSpeed->Speed_pr;

        if (temp_pos32bval > _IQ(1))
        {
            posSpeed->Speed_pr = _IQ(1);
        }
        else
        {
            posSpeed->Speed_pr = temp_pos32bval;
        }

        /*
         * Convert p->Speed_pr to RPM
         * */

        /*
         * Reverse direction = negative
         * */
        if (posSpeed->DirectionQep == 0)
        {
            /*
             * Q0 = Q0*GLOBAL_Q => _IQXmpy(), X = GLOBAL_Q
             * */
            posSpeed->SpeedRpm_pr = -_IQmpy(posSpeed->BaseRpm, posSpeed->Speed_pr);
        }

        else    /*Forward direction = positive*/
        {
            /*
             * Q0 = Q0*GLOBAL_Q => _IQXmpy(), X = GLOBAL_Q
             * */
            posSpeed->SpeedRpm_pr = _IQmpy(posSpeed->BaseRpm, posSpeed->Speed_pr);
        }

        /*
         * Clear Unit position event flag; Clear overflow error flag
         * */
        EQep1Regs.QEPSTS.all=0x88;
    }
}
