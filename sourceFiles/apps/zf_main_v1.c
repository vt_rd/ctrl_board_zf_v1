/******************************************************************************
 *
 * Copyright (c) 2018
 * Viettel R&D.
 * All Rights Reserved
 *
 *
 * Description: Control position, speed of one DC Motor Maxon with response of
 * position and speed from Encoder on the Motor.
 *
 * Author:  QuangHuy
 *
 * Last Changed By:  $Author:     QuangHuy
 * Revision:         $Revision:   1.0 $
 * Last Changed:     By QuangHuy  $Date: 18/6/2018     11:59:00
 *
 ******************************************************************************/

/******************************************************************************/
/*                              INCLUDE FILES                                 */
/******************************************************************************/
#include "board.h"
#include "zf_mainv1.h"
/******************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                         */
/******************************************************************************/
#ifdef  ZF_MAIN
#define ZF_MAIN_SEND_CHAR(c)        SCIA_SendChar(c)
#define ZF_MAIN_SEND_STR(STR)       SCIA_SendString(STR)
#define ZF_MAIN_SEND_NUM(num)       SCIA_SendInt(num)
#define ZF_MAIN_SEND_NL()           SCIA_SendNL()
#else
#define ZF_MAIN_SEND_CHAR(c)
#define ZF_MAIN_SEND_STR(STR)
#define ZF_MAIN_SEND_NUM(num)
#define ZF_MAIN_SEND_NL()
#endif
/******************************************************************************/
/*                              PRIVATE DATA                                  */
/******************************************************************************/

/******************************************************************************/
/*                              EXPORTED DATA                                 */
/******************************************************************************/

/******************************************************************************/
/*                            PRIVATE FUNCTIONS                               */
/******************************************************************************/
PosSpeed_t qep_posspeed1 = POSSPEED_DEFAULTS;
/******************************************************************************/
/*                            EXPORTED FUNCTIONS                              */
/******************************************************************************/
/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void CmdHandler(void_p, void_p cmd);
/******************************************************************************/
void sendTemperatureToPC(void_p px1, void_p px2)
{
    static u16_t temperature = 0;
    static u16_t old_temperature = 0;

    temperature = LM35_GetTemperature0();

    if(temperature != old_temperature)
    {
        old_temperature = temperature;

        ZF_MAIN_SEND_STR("Temperature: ");
        ZF_MAIN_SEND_NUM(temperature);
        ZF_MAIN_SEND_NL();
    }
}

interrupt void Speed_Handler(void)
{
    //MOTOR_StabilizeSpeed((void_p)MOTOR_ZOOM, (void_p)1000);
    MOTOR_StabilizePosition((void_p)MOTOR_ZOOM, (void_p)-32000);
}

/**
 * @func
 *
 * @brief  None
 *
 * @param  None
 *
 * @retval None
 */
void main(void)
{
    BOARD_Init();

    MOTOR_Direction((void_p)MOTOR_ZOOM, (void_p)MOTOR_CW);
    MOTOR_Enable((void_p)MOTOR_ZOOM, (void_p)MOTOR_ENABLE);

    TimerStart(1000, TIMER_FOREVER, sendTemperatureToPC, NULL, NULL);

    while(1)
    {
        processTimer();
    }
}

void CmdHandler(void_p motor, void_p cmd)
{
    motorCmd_t m_cmd = (motorCmd_t)cmd;

    switch(m_cmd)
    {
        case MOTOR_ENABLE:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Enable, (void_p)MOTOR_ZOOM, (void_p)MOTOR_ENABLE);
            break;
        case MOTOR_DISABLE:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Enable, (void_p)MOTOR_ZOOM, (void_p)MOTOR_DISABLE);
            break;
        case MOTOR_CW:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Direction, (void_p)MOTOR_ZOOM, (void_p)MOTOR_CW);
            break;
        case MOTOR_CCW:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Direction, (void_p)MOTOR_ZOOM, (void_p)MOTOR_CCW);
            break;
        case MOTOR_MAX_SPEED:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_MaxSpeed, (void_p)MOTOR_ZOOM, NULL);
            break;
        case MOTOR_MIN_SPEED:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_MinSpeed, (void_p)MOTOR_ZOOM, NULL);
            break;
        case MOTOR_INCREASE_SPEED:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_IncreaseSpeed, (void_p)MOTOR_ZOOM, NULL);
            break;
        case MOTOR_DECREASE_SPEED:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_DecreaseSpeed, (void_p)MOTOR_ZOOM, NULL);
            break;
        case MOTOR_SPEED_10:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)10);
            break;
        case MOTOR_SPEED_20:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)20);
            break;
        case MOTOR_SPEED_30:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)30);
            break;
        case MOTOR_SPEED_40:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)40);
            break;
        case MOTOR_SPEED_50:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)50);
            break;
        case MOTOR_SPEED_60:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)60);
            break;
        case MOTOR_SPEED_70:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)70);
            break;
        case MOTOR_SPEED_80:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)80);
            break;
        case MOTOR_SPEED_90:
            TimerStart(0, TIMER_ONE_TIME, MOTOR_Speed, (void_p)MOTOR_ZOOM, (void_p)90);
            break;
        default:
            ZF_MAIN_SEND_STR("\nUnknown CMD!\n\0");
            break;
    }
}

/* END FILE */
